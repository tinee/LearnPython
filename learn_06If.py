
"""
* if语句和缩进部分是一个完整的代码块
"""

age = int(input("请输入年龄："))
if age >= 18:
    print("您已成年")
elif age > 0 and age < 18:  #或者 0 < age < 18
    print("您还未成年")
    print("else部分1")
    print("else部分2")
else:
    print("输入年龄非法")
print("这部分与if代码块不相关，每次都会执行。")
