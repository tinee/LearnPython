
# 无限循环
import cards_tools

while True:
    # TODO 显示功能菜单
    cards_tools.show_menu()
    action_str = input("请选择需要执行的操作：")
    # 1,2,3针对名片的操作
    if action_str in ["1", "2", "3"]:
        # 新增名片
        if action_str == "1":
            cards_tools.new_card()
        # 显示全部
        elif action_str == "2":
            cards_tools.show_all()
        # 查询名片
        else:
            cards_tools.query_card()
    # 退出系统
    elif action_str == "0":
        # 如果在开发系统时，不希望立刻编写分支内部代码
        # 可以使用 pass 关键字，表示一个占位符，能够保证程序代码的结构正确。
        # pass
        print("欢迎再次使用")
        break
    else:
        print("您输入的【%s】不正确，请重新输入" % action_str)
