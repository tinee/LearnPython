"""
"""

for item in [31, 15, 23]:
    print(item)
print("循环结束")
print("*************************************")
for item in [31, 15, 23]:
    print(item)
else:
    print("当循环当中没有'break'，会执行这一句话")
print("循环结束")
print("*************************************")
for item in [31, 15, 23]:
    print(item)
    if item == 15:
        break
else:
    print("当循环当中没有'break'，会执行这一句话")
print("循环结束")