# 定义一个列表
name_list = ["张三", "李四", "tom"]
print("****************************索引********************************")
# 索引
print(name_list.index("李四"))
# 根据索引查值
print(name_list[2])
print("****************************修改********************************")
# 修改
name_list[2] = "jarry"
print(name_list[2])
print("****************************新增********************************")
# 新增
name_list.append("lucy")
name_list.insert(2, "王五")

extend_list = ["小明", "王刚"]
name_list.extend(extend_list)
print(name_list)
print("****************************删除********************************")
# 删除
name_list2 = ["迷你", "小小", "中中", "大大", "特特"]
name_list2.remove("特特")
print(name_list2)
name_list2.pop()    #pop方法默认删除最后一个，若指定参数，则删除这个索引的值
name_list2.pop(2)
print(name_list2)

del name_list2[1]   #del本质上把一个变量从内存中删除
print(name_list2)
name_list2.clear()    #删除所有
print(name_list2)
print("**************************长度、次数******************************")
name_list3 = ["AA", "BB", "CCC", "D", "EEEE", "D"]
list_len = len(name_list3)
print("长度：%d" % list_len)
list_count = name_list3.count("D")
print("\"D\"出现了：%d次" % list_count)
print("****************************排序********************************")
name_list3 = ["AA", "BB", "CCC", "D", "EEEE", "D"]
name_list4 = [3, 2, 36, 18, 77, 25]
name_list3.sort()   #升序
name_list4.sort()
print(name_list3)
print(name_list4)

name_list3 = ["AA", "BB", "CCC", "D", "EEEE", "D"]
name_list4 = [3, 2, 36, 18, 77, 25]
name_list3.sort(reverse=True)    #降序
name_list4.sort(reverse=True)
print(name_list3)
print(name_list4)

name_list3 = ["AA", "BB", "CCC", "D", "EEEE", "D"]
name_list4 = [3, 2, 36, 18, 77, 25]
name_list3.reverse()    #反转
name_list4.reverse()
print(name_list3)
print(name_list4)