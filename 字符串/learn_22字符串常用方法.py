info_str = "hello my pretty girl!"
space_str = ""

# 判断是否为空白字符()
print(info_str.isspace())
print(space_str.isspace())
print("**************************")
# 判断字符串是否只包含数字
num_str = "236444367"
print(num_str.isdecimal())  #开发时应该使用这个判断数字
print(num_str.isdigit())    #可判断(1)
print(num_str.isnumeric())  #可判断全角数字、中文数字
print("**************************")

# 判断是否以指定的字符串开始
print(info_str.startswith("h"))
print(info_str.startswith("hello"))
print(info_str.startswith("Hello"))
# 判断是否以指定的字符串结束
print(info_str.endswith("!"))
print(info_str.endswith("girl!"))
print(info_str.endswith("Girl!"))
# 查找指定的字符串
print(info_str.find("girl"))    #16:索引，从0开始
print(info_str.rfind("girl"))   #从右边查询。16:索引，从0开始
print(info_str.rfind("kkk"))    #不存在。-1：索引
# 替换字符串
print(info_str.replace("girl", "boy"))