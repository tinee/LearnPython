"""
字符串
    定义：""或''定义，建议使用""定义
    长度：len()
    计数：count()
    位置：index
"""
str = "hello python"
print(str)
for char in str:
    print(char)
print("**************************************")
# 长度：len()
print(len(str))

# 计数：count()
print(str.count("o"))

# 位置：index
print(str.index("llo"))
# 如果传递的子字符串在查询的字符串中不存在，则会报错！
print(str.index("kkk"))