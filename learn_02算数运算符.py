print(5+2)
print(5-2)
print(5*2)
print(5/2)
print(5//2) #整除
print(5%2)  #取余
print(5**2)  #幂
"""
在python中，*运算还可用于字符串，结果为字符串重复拼接指定次数的结果。
效果如下：
"""
print("Cc" * 5)