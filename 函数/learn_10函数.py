

def sayHello():
    """
    打招呼函数
    :return:
    """
    print("hello!")


def add(num1, num2):
    """
    两个整数相加
    :param num1:
    :param num2:
    :return: 和
    """
    result = num1 + num2
    # print("%d + %d = %d" % (num1, num2, result))
    return result