name = "tom"
print("姓名为%s, 请完善信息。" % name)

age = 18
print("年龄为%d岁, 请完善信息。" % age)
no = 125
print("工号为%06d, 请完善信息。" % no)  #06d：6位整数，空的用0补齐

weight = 55.2
print("体重为%.3f公斤, 请完善信息。" % weight)  #.3f浮点，小数点后显示三位

# 输入10，输出为10.00%
scale = print("%.2f%%" % float(input("请输入：")))