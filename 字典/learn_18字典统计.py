dic = {"name":"tom", "age":18, "weight":57.5}

# 统计键值对数量
print(len(dic))
# 合并字典
# 如果被合并的字典中包含已存在的键值对，会覆盖原有的键值对
dic_ext = {"job":"doctor", "age":22}
dic.update(dic_ext)
print(dic)
# 清空字典
dic.clear()
print(dic)