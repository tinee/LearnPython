"""
字典：
    定义：{}
"""
dic = {"name":"tom", "age":18, "weight":57.5}
# 取值
print(dic)
print(dic["name"])
# 增加/修改
dic["height"] = 178 #key存在，则修改值
dic["gender"] = "0" #key不存在，则新增值
print(dic)
# 删除
dic.pop("name")
print(dic)