"""
将多个字典 放在 一个列表中
"""
card_list = [
    {"name":"tom","gender":"男"},
    {"name":"jarry","gender":"男"},
    {"name":"lucy","gender":"女"},
]

for dic in card_list:
    print(dic)