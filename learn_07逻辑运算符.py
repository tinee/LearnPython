
"""
逻辑运算符 and or not

"""

age = int(input("请输入年龄："))
sex = input("请输入性别：")
gender = False
if sex == "男":
    gender = True
else:
    gender = False
if age >= 18 and gender == True:
    print("您已成年，并是男性")
if age >= 18 and gender == False:
    print("您已成年，并是女性")
if age < 18 and gender == True:
    print("您未成年，并是男性")
if age < 18 and gender == False:
    print("您未成年，并是女性")