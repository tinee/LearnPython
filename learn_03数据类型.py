"""
Python3 中有六个标准的数据类型：
    Number（数字）
    String（字符串）
    List（列表）
    Tuple（元组）
    Set（集合）
    Dictionary（字典）

* python定义变量时不需要指定变量的类型
* type()判断数据类型
"""
# 整型
num1 = 1
num2 = 3
# 字符串类型
a = "hello"
b = "world"
# 布尔类型
gender = False
# 浮点型
weight = 52.20

print(a+b)
print(num1 + num2)
print(a + str(num1))    #int转string

# 判断数据类型
print(type(a))
print(type(num1))
print(type(gender))
print(type(weight))


# list
list1 = [1, 2, 3]
for i in list1:     #for循环
    print("列表的值:" + str(i))

"""
Tuple（元组）
元组（tuple）与列表类似，不同之处在于元组的元素不能修改。元组写在小括号 () 里，元素之间用逗号隔开。
"""
tuple = ('abc', 786 , 2.23, 'EFG', 70.2)
tinytuple = (123, 'EFG')

print(tuple)  # 输出完整元组
print(tuple[0])  # 输出元组的第一个元素
print(tuple[1:3])  # 输出从第二个元素开始到第三个元素
print(tuple[2:])  # 输出从第三个元素开始的所有元素
print(tinytuple * 2)  # 输出两次元组
print(tuple + tinytuple)  # 连接元组


"""
Set（集合）
集合（set）是一个无序不重复元素的序列。
基本功能是进行成员关系测试和删除重复元素。
可以使用大括号 { } 或者 set() 函数创建集合，注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。
"""
set1 = {1, 1, 2, 3}
set2 = set([1, 1, 2, 3])
print(set1) #会去重
print(set2) #会去重

"""
Dictionary（字典）
字典（dictionary）是Python中另一个非常有用的内置数据类型。
列表是有序的对象集合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。
字典是一种映射类型，字典用"{ }"标识，它是一个无序的键(key) : 值(value)对集合。
键(key)必须使用不可变类型。
在同一个字典中，键(key)必须是唯一的。
"""
dict = {}  #定义字典
dict['name'] = "tom"
dict['age'] = 18
dict[666] = '666'

tinydict = {'name': 'tom', 'age': 18, 666: '666'}

print("输出键为 'name' 的值:" + dict['name'])  # 输出键为 'name' 的值
print(dict[666])  # 输出键为 666 的值
print(tinydict)  # 输出完整的字典
print(tinydict.keys())  # 输出所有键
print(tinydict.values())  # 输出所有值