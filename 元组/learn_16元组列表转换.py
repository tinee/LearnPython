"""
元组转列表：list()
列表转元组：tuple()
"""
# 元组转列表
info_tuple = ("tom", 18, 52.5, 178.0)
result_list = list(info_tuple)
print(result_list)

# 列表转元组
info_list = ["tom", 18, 52.5, 178.0]
result_tuple = tuple(info_list)
print(result_tuple)