"""
Tuple元组：
与列表不同之处：元组的元素不能修改
定义：用()定义

应用场景：

"""
info_tuple = ("tom", 18, 15.75)
print(info_tuple)
print(info_tuple[0])

# 定义空元组
empty_tuple = ()
print(empty_tuple)

# 定义一个元素的元组,需要在元素后面添加逗号！！！
one_tuple = (5,)

print(one_tuple)
