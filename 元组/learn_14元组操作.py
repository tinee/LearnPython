"""
Tuple元组：
    count：出现次数
    index：索引
"""
info_tuple = ("tom", 18, 15.75, 18)

# 取值、取索引
print("\"15.75\"的索引：%d" % info_tuple.index(15.75))
print("索引0的值：%s" % info_tuple[0])

# 统计计数
print("\"18\"出现次数%d" % info_tuple.count(18))

# 统计元素个数
print("元素个数：%d" % len(info_tuple))
